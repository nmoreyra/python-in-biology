# Programación en python para biología 2019
**Universidad Favaloro**
* Sede: Sarmiento 1853
* Cursado: 8 clases los viernes de 13 a 17hs.
* Período: desde el 6 de septiembre hasta el 25 de octubre.
* Aula: a definir
###   Links Útiles:
* :computer:[Cómo instalar la Máquina Virtual](COMO_INSTALAR_LA_VM.md)
* :cd:[Instalar Programas Necesarios](https://gitlab.com/nmoreyra/python-in-biology/blob/master/INSTALAR_PROGRAMAS_NECESARIOS.md)
___
## Contenidos del curso
* #### [Clase I.](https://gitlab.com/nmoreyra/python-in-biology/tree/master/Clase_I) Configuración inicial
----
* #### [Clase II.](https://gitlab.com/nmoreyra/python-in-biology/tree/master/Clase_II) Scripting y control de versiones
----
* #### [Clase III.](https://gitlab.com/nmoreyra/python-in-biology/tree/master/Clase_III) Python: Variables y control de flujo
----
* #### [Clase IV.](https://gitlab.com/nmoreyra/python-in-biology/tree/master/Clase_IV) Python: Funciones, manejo de archivos e interpretación de errores
----
* #### [Clase V.](https://gitlab.com/nmoreyra/python-in-biology/tree/master/Clase_V) Python: Manejo de datos I
----
* #### [Clase VI.](https://gitlab.com/nmoreyra/python-in-biology/tree/master/Clase_VI) Python: Manejo de datos II
----
* #### [Clase VIII.](https://gitlab.com/nmoreyra/python-in-biology/tree/master/Clase_VII) Python: Visualización de datos
----
* #### [Clase VIII.](https://gitlab.com/nmoreyra/python-in-biology/tree/master/Clase_VIII) Proyecto supervisado: Análisis e Interpretación de datos

----
* #### :books: Bibliografía optativa 
 * [VV. AA. Software Carpentry: La Terminal de Unix.](https://swcarpentry.github.io/shell-novice-es/)
 * [VV. AA. Software Carpentry: El Control de Versiones con Git.](https://swcarpentry.github.io/git-novice-es/)
 * [Ekmekci B, McAnany CE, Mura C. An Introduction to Programming for Bioscientists: A Python-Based Primer. PLoS Comput Biol. 2016 Jun 7;12(6):e1004867.](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004867)
 * [Bahit E. Curso: Python para Principiantes.](https://www.iaa.csic.es/python/curso-python-para-principiantes.pdf)
 * [Jones M. Python for Biologists.](http://pythonforbiologists.com/)
 * [White E. Programming for Biologists.](http://www.programmingforbiologists.org/material/)
 * [Pratusevich M. Practice Python.](http://www.practicepython.org/)
 * [Kirienko D. et al. Snakify.](http://snakify.org/)
 * [Müller M. Scientific Plotting with Matplotlib.](http://www.python-academy.com/download/pycon2012/matplotlib_handout.pdf)
 * [Varios autores. Matplotlib Documentation.](http://matplotlib.org)
 * [Varios autores. Seaborn Documentation.](http://seaborn.pydata.org)
 * [McKinney W, PyData Development Team. Pandas: powerful Python data analysis toolkit.](https://pandas.pydata.org/pandas-docs/stable/pandas.pdf)
 * [Varios autores. Pandas Documentation.](https://pandas.pydata.org/pandas-docs/stable/)
 * [Varios autores. Biopython Documentation.](http://biopython.org/wiki/Documentation)
 * [Varios autores. Biopython Tutorial and cookbook.](http://biopython.org/DIST/docs/tutorial/Tutorial.pdf)
 * [Vos R. Bio::Phylo Documentation.](https://informatics.nescent.org/w/images/5/57/BioPhylo.pdf)
 * [Huerta-Cepas J, Serra F, Bork P. ETE Toollkit.](http://etetoolkit.org/)
 * [Varios Autores. Lasagne documentation.](https://media.readthedocs.org/pdf/lasagne/latest/lasagne.pdf)
 * [Varios autores. Python Enhancement Proposals (PEPs).](https://www.python.org/dev/peps/)
