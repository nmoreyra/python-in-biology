* #### Clase_VIII - Proyecto supervisado: Análisis e Interpretación de datos
	*   Planteo de proyectos individuales.
	*   Formulación de hipótesis y esquema de trabajo.
	*   Obtención de datos crudos.
	*   Limpieza de datos.
	*   Conversión a formatos procesables.
	*   Estadística descriptiva y avanzada.
	*   Generación e interpretación de gráficos.
		* [Eligiendo un buen gráfico](choosing-a-good-chart-09.pdf)
	*   Interpretación de resultados.
	*   Puesta en común de proyectos: factibilidad de análisis, sugerencias.
	*   Conclusiones generales.
	*   Directivas para informe final.
