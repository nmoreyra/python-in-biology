## Probando blast de forma local.

Si googleamos `blast linux download` problablemente el primer resultado sea la dirección desde donde podemos descargar los archivos ejecutables para correr **blast** en nuestras computadoras ([Descargar Blast+](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download)). Sin embargo, Uds ya cuentan con el software instalado en sus ordenadores (VM), donde tienen disponibles todas las aplicaciones, incluso aquellas necesarias para formatear las bases de datos a utilizar antes de realizar el **blast**.
Cualquier programa puede ser ejecutado simplemente llamando su nombre desde la terminal, como se verá más adelante.

La (sub)carpeta `secuencias_blast`, ubicada dentro de la carpeta `Clase_II`, posee el archivo `uniprot_human_prot_rev.fasta`, que contiene alrededor de 39.000 secuencias proteicas humanas obtenidas de Uniprot (http://www.uniprot.org/).
Por otro lado, también puede encontrar el archivo `query.fas`, el cuál posee una secuencia de nucleótidos de mRNA (ARN mensajero) expresada en un tejido de cáncer de pulmón humano. Antes de "*blastear*" (acción de realizar un **blast**), es necesario crear una base de datos a partir de las secuencias contra las cuales se quiere comparar nuestra secuencia de consulta (*query*).

Estando ubicado sobre la carpeta `/home/alumno/Clase_II/`, ver el contenido de la carpeta `secuencias_blast`. Luego, cree una carpeta llamada **trabajo**, donde pondremos las salidas de nuestro análisis e ingrese a la misma.

```bash
cd /home/alumno/Clase_II/
ls secuencias_blast
mkdir trabajo #creamos una carpeta donde trabajar y colocar las salidas de las corridas de `blast`
cd trabajo
pwd #si desea checkear su ubicación
```

Ahora debemos crear la base de datos con las secuencias de Uniprot mediante el comando `makeblastdb`.

```bash
ls ../secuencias_blast #volvemos a ver las secuencias mediante la ruta relativa a su carpeta
makeblastdb -in ../secuencias_blast/uniprot_human_proteins_reviewed.fasta -dbtype prot -out ../secuencias_blast/db_prot_human
```

`blastx` es un subprograma de **blast** que traduce la secuencia query (que es nucleotídica) en sus seis posibles marcos de lectura (tres marcos de lecturas por hebra) y compara estas secuencias traducidas contra una base de datos de proteínas.
Utilizar `blastx` y explorar los resultados.

```bash
blastx -db ../secuencias_blast/db_prot_human -query ../secuencias_blast/query.fas -out mi_primer_blast.txt
```

Investigar el archivo de salida. ¿Qué información se obtiene?

```bash
ls -lh mi_primer_blast.txt
wc -l mi_primer_blast.txt
less -S mi_primer_blast.txt
```

¿Es posible presentar los resultados en formato de tabla?
*Hint: investigar los diferentes formatos de salidas de blast (blastx -help)*

```bash
blastx -help
blastx -db ../secuencias_blast/db_prot_human -query ../secuencias_blast/query.fas -out mi_primer_blast.tabla -outfmt 6
```

¿Cuantas salidas (secuencias de comparación) tiene el archivo **mi_primer_blast.tabla**?

```bash
less -S mi_primer_blast.tabla
wc -l mi_primer_blast.tabla > numero_de_salidas_blastx.txt
```

