# Filtros sed y awk

## sed (Stream-oriented, Non-Interactive, Text Editor)
Sed es considerado un editor de texto orientado a "flujo" (en contraposición a los clásicos editores clásicos). sed acepta como entrada un archivo o la salida de otro comando y procesa de a una línea a la vez, y el resultado es enviado a la salida estándar (por la terminal de comandos).
Busca patrones en las líneas del _input_, al igual que grep, pero puede cambiar el contenido de aquellas líneas que cumplan con el patrón de búsqueda.

### Formato de uso
El formato básico de uso de sed es:
`sed [-ns] '[direccion] instruccion argumentos' `

Donde,

* **[direccion]** es opcional, siendo un número de línea (N), rango de números de línea (N,M) o búsqueda de expresion regular (/cadena/) indicando el ámbito de actuación de las instrucciones. Si no se especifica **[direccion]**, se actúa sobre todas las líneas del flujo.
* **instruccion** puede ser:
	* i = Insertar línea antes de la línea actual.
	* a = Insertar línea después de la línea actual.
	* c = Cambiar línea actual.
	* d = Borrar línea actual.
	* p = Imprimir línea actual en stdout.
	* s = Sustituir cadena en línea actual.
	* r fichero = Añadir contenido de "fichero" a la línea actual.
	* w fichero = Escribir salida a un fichero.
	* ! = Aplicar instrucción a las líneas no seleccionadas por la condición.
	* q = Finalizar procesamiento del fichero.
	* -n: No mostrar por stdout las líneas que están siendo procesadas.
	* -s: Tratar todos los ficheros entrantes como flujos separados.

Para comenzar a utilizar **sed**, primero ubiquemonos en la carpeta `/home/alumno/Clase_II/trabajo`, donde se encuentran las salidas de blast. Revisemos, nuevamente, el formato del archivo:

```bash
cd ~/Clase_II/trabajo
less -S salida_blastx_script.table
```

Sustituir apariciones de "**query_sequence**" por "**mi_secuencia**" en todo el fichero, poniendo los cambios en un nuevo archivo. Revíselo:

```bash
sed 's/query_sequence/mi_secuencia/g' salida_blastx_script.table > salida_blastx_script.auxiliar
less -S salida_blastx_script.auxiliar
```

Ahora, sustituir apariciones de "**query_sequence**" por "**mi_secuencia**" enentre las líneas 1 y 10. Sobreescriba el archivo generado anteriormente:

```bash
sed '1,10  s/query_sequence/mi_secuencia/g' salida_blastx_script.table > salida_blastx_script.auxiliar
less -S salida_blastx_script.auxiliar
```

Como ya sabemos que el archivo contiene 13 líneas, ahora podemos eliminar las últimas dos líneas (11, 12 y 13):
```bash
sed '11,13 d' salida_blastx_script.auxiliar
```

Como podrá ver, si no redireccionamos la salida del comando **sed**, la salida es mostrada por la terminal. Pruebe ingresando el _flag_ `-i` antes de los cambios que desea realizar:

```bash
sed -i '11,13 d' salida_blastx_script.auxiliar
```
Por último, si quisieramos agregar un encabezado a la tabla, de modo que se entiendan los campos, podríamos hacerlo de la siguiente manera:

`qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue bitscore`

```bash
sed -i '1i seq_q\tseq_db\tid%\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore' salida_blastx_script.auxiliar
```
```bash
alumno@cursopython:~$ sed -help
sed: invalid option -- 'h'
Usage: sed [OPTION]... {script-only-if-no-other-script} [input-file]...

  -n, --quiet, --silent
                 suppress automatic printing of pattern space
  -e script, --expression=script
                 add the script to the commands to be executed
  -f script-file, --file=script-file
                 add the contents of script-file to the commands to be executed
  --follow-symlinks
                 follow symlinks when processing in place
  -i[SUFFIX], --in-place[=SUFFIX]
                 edit files in place (makes backup if SUFFIX supplied)
  -l N, --line-length=N
                 specify the desired line-wrap length for the 'l' command
  --posix
                 disable all GNU extensions.
  -E, -r, --regexp-extended
                 use extended regular expressions in the script
                 (for portability use POSIX -E).
  -s, --separate
                 consider files as separate rather than as a single,
                 continuous long stream.
      --sandbox
                 operate in sandbox mode.
  -u, --unbuffered
                 load minimal amounts of data from the input files and flush
                 the output buffers more often
  -z, --null-data
                 separate lines by NUL characters
      --help     display this help and exit
      --version  output version information and exit

If no -e, --expression, -f, or --file option is given, then the first
non-option argument is taken as the sed script to interpret.  All
remaining arguments are names of input files; if no input files are
specified, then the standard input is read.

GNU sed home page: <http://www.gnu.org/software/sed/>.
General help using GNU software: <http://www.gnu.org/gethelp/>.
```

##  
## awk
El comando awk o GNU awk en específico proporciona un lenguaje de scripting para el procesamiento de texto. Con el lenguaje de scripting awk puedes:
1) Definir variables.
2) Utilizar cadenas y operadores aritméticos.
3) Utilizar control de flujo y ciclos.
4) Generar reportes con formato.

Utilizando Variables:
Awk asigna algunas variables para cada campo de datos encontrado:

$0 para toda la línea.
$1 para el primer campo.
$2 para el segundo campo.
$n para el campo enésimo campo.

Es decir, el comando awk permite imprimir una columna específica de un archivo. Por ejemplo, ubíquese en `~/Clase_II/trabajo`, para imprimir con el comando **awk** la segunda y tercera columna del archivo **salida_blastx_script.table**:

```bash
awk '{print $2" "$3}' salida_blastx_script.table
```

Tenga en cuenta que necesita prestar atención al caracter de delimitación utilizado en el archivo. En este caso, el archivo es tabulado, lo cual es detectado automáticamente por awk.
Si el archivo estuviera delimitado por comas, awk no sería capaz de diferenciar entre columnas. Probemos esto, realice las siguientes acciones utilizando el comando aprendido previamente, **sed**. 

```bash
cat salida_blastx_script.table
sed 's/\t/,/g' salida_blastx_script.table > salida_blastx_script.csv
cat salida_blastx_script.csv
```

Por lo tanto, para utilizar awk con este archivo, se debe indicar cuál es el caracter de delimitación. Esto se hace utilizando el argumento ‘-F’:

```bash
awk -F "," '{print $2" "$3}' salida_blastx_script.csv
```

Otro ejemplo con un archivo del sistema. El archivo /etc/passwd determina quien puede acceder al sistema de manera legitima y que se puede hacer una vez dentro del sistema.


```bash
cat /etc/passwd
awk -F: '{print $1}' /etc/passwd
```

Utilizando múltiples comandos, modificamos la salida del standard input:

```bash
echo "Hello Tom" | awk '{$2="Adam"; print $0}
```

Ahora intentemos modificar un la tabla **salida_blastx_script.table**:

```bash
awk 'BEGIN {print "Esta es una tabla de blastx:"}
{print $0}' salida_blastx_script.table
```

En caso que querramos realizar una acción mientras procesamos un archivo:

```bash
awk 'BEGIN {print "Esta es una tabla de blastx:"} {if ($11 < 0.5) print $0} END {print "Estos son los hits con E-value < 0.5"}' salida_blastx_script.table
```

Por último, veremos como trabajar y operar entre columnas. Supongamos que queremos restar dos columnas numericas y dar como salida la resta:

```bash
awk '{if ($11 < 0.5) print $0,$4-$5}' salida_blastx_script.table
```

Hay muchas más tareas que pueden realizarse con este potente comando, y mucho más complejas, pero por ahora nos quedaremos con estos ejemplos.







