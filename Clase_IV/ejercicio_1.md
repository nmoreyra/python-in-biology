# Funciones

Adaptado de [Bioinformatics at COMAV: Funciones](https://bioinf.comav.upv.es/courses/linux/python/funciones.html)

## Ejercicio 1

El siguiente programa para calcular el área de un triángulo. Sin embargo, el programa no funciona. ¿Puede corregirlo? (Pista: sólo debe agregar una línea).

```
from math import sqrt
def calcular_area_triangulo(lado1, lado2, lado3):
    semiperimetro = (lado1 + lado2 + lado3) / 2.0
    area2 = semiperimetro * (semiperimetro - lado1) * (semiperimetro - lado2) * (semiperimetro - lado3)
    area = sqrt(area2)

print(calcular_area_triangulo(4, 5, 3))
calcular_area_triangulo(1, 2, 3)

```
## Ejercicio 2

Escriba un programa que calcule el área de un triángulo utilizando la fórmula: base * altura / 2.

## Ejercicio 3

Escriba un programa con una función que tome una lista (de números) y sume sus elementos.

