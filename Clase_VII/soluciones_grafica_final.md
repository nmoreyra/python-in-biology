Primero agrupemos los datos por sitio y por sexo y después calculemos el total para cada sitio.

```
by_site_sex = surveys_df.groupby(['plot_id','sex'])
site_sex_count = by_site_sex['weight'].sum()
```


Esto calcula la suma de los pesos para cada sexo por sitio como una tabla

```
site  sex
plot_id  sex
1        F      38253
         M      59979
2        F      50144
         M      57250
3        F      27251
         M      28253
4        F      39796
         M      49377
<lo demás fue omitido para abreviar>
```


Ahora usaremos `.unstack()` en los datos agrupados para entender como el peso total de cada sexo contribuye a cada sitio.

```
by_site_sex = surveys_df.groupby(['plot_id','sex'])
site_sex_count = by_site_sex['weight'].sum()
site_sex_count.unstack()
```


El método `unstack` de arriba despliega la siguiente salida:

```
sex          F      M
plot_id
1        38253  59979
2        50144  57250
3        27251  28253
4        39796  49377
<los demás sitios son omitido por brevedar>
```


Ahora creamos una gráfica de barras apilada con los datos donde el peso para cada sexo es apilado por sitio.

En vez de mostrarla como tabla, nosotros podemos graficar los datos apilando los datos de cada sexo como sigue:

```
by_site_sex = surveys_df.groupby(['plot_id','sex'])
site_sex_count = by_site_sex['weight'].sum()
spc = site_sex_count.unstack()
s_plot = spc.plot(kind='bar',stacked=True,title="Total weight by site and sex")
s_plot.set_ylabel("Weight")
s_plot.set_xlabel("Plot")
```

![imagen resultados grafica final](https://datacarpentry.org/python-ecology-lesson-es/fig/stackedBar.png)