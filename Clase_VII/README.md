* #### Clase VII. Python para Biología
	* [Combinando DataFrames con Pandas](https://datacarpentry.org/python-ecology-lesson-es/05-merging-data/index.html)
	* Trabajando con gráficos
		* [Visualización con Matplotlib y Pandas](https://datacarpentry.org/python-ecology-lesson-es/08-putting-it-all-together/index.html)
		* [Google Colab notebook de clase](https://colab.research.google.com/drive/1A1T_0oetLtoHmOkB1hw0xo_lX3Phhmm4)
		* [Cargar datos en Google Colab](https://towardsdatascience.com/3-ways-to-load-csv-files-into-colab-7c14fcbdcb92)
		* [Matplotlib básico](https://towardsdatascience.com/matplotlib-tutorial-learn-basics-of-pythons-powerful-plotting-library-b5d1b8f67596)
	* Conceptos adicionales
		* [Manejo de errores con try-except](https://www.w3schools.com/python/python_try_except.asp)
		* [Trucos en Python: reversión de strings, list comprehension, lambda & map, etc](https://towardsdatascience.com/python-tricks-101-what-every-new-programmer-should-know-c512a9787022)
	* Análisis de datos biológicos: Biopython. 
	* Manejo de datos genómicos (genomas, anotaciones, lecturas de secuenciación en formato fastq, etc) para el cálculo de estadísticas básicas (estándares).
	* Scipy: métodos estadísticos.
	* Filogenia: Bio.Phylo; ETE Toolkit.
* ##### Ejercicios de gráficas :space_invader:
    * [Descripción de datos ](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_VII/Reto_descripcion_de_datos.md)
        * [Solución](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_VII/Soluci%C3%B3n_Reto_descripcion_de_datos.py) 
    * [Reto-Gráficas](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_VII/Ejercicio_Gr%C3%A1ficas.md)
        * [Solución](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_VII/Soluci%C3%B3n_Ejercicio_Gr%C3%A1ficas.py) 
    * [Gráficos - final](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_VII/Ejercico_%22Graficaci%C3%B3n%22_final.md) 
        * [Solución py](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_VII/Soluci%C3%B3n_Ejercico_Graficaci%C3%B3n_final.py)
        * [Solución y explicación en .md](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_VII/Ejercico_%22Graficaci%C3%B3n%22_final.md)
    * [Distribuciones e indice de biodiversidad](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_VII/Ejercicio_gr%C3%A1fico_de_distribuciones_e_%C3%ADndice_de_diversidad.md)
        * [Solución](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_VII/Soluciones_Ejercicio_gr%C3%A1fico_de_distribuciones_e_%C3%ADndice_de_diversidad.py) 
    * [Integración](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_VII/Ejercicios_de_integracion.md)
        * [Solución](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_VII/Soluci%C3%B3n_Ejercicios_de_integracion.py) 

Las tablas usadas en los ejercicios se encuentran en la [carpeta de la Clase_V](https://gitlab.com/nmoreyra/python-in-biology/tree/master/Clase_V)