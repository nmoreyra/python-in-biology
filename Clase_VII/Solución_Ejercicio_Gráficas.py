import pandas as pd
import matplotlib.pyplot as plt
pd.set_option('display.expand_frame_repr', False)
surveys_df = pd.read_csv(r"C:\Users\Nahuel\Desktop\surveys.csv") #el 'r' y la barra invertida es solo para Windows
# 1) Crea una gráfica del promedio de peso de las especies por sitio.
species_prom= surveys_df.groupby('plot_id')['weight'].mean()
#print(species_prom)
#print(species_prom.plot(kind='bar'));
#plt.show()
# 2) Crea una gráfica del total de machos contra el total de hembras para todo el conjunto de datos.
#promedios
agrupados = surveys_df.iloc[:, 1:] #selecciono todas las columnas menos el "record_id" que tiene numeros muy  altos
agrupados_por_sexo = agrupados.groupby("sex")
#print(agrupados_por_sexo.mean())
#print(agrupados_por_sexo.mean().plot(kind= "bar"));
#plt.show()
#conteo total
#print(agrupados_por_sexo.count())
print(agrupados_por_sexo.count().plot(kind= "bar"));
plt.show()
