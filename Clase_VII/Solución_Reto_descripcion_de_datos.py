import pandas as pd
import matplotlib.pyplot as plt
surveys_df = pd.read_csv(r"C:\Users\Nahuel\Desktop\surveys.csv")
pd.set_option('display.expand_frame_repr', False) #esto deshabilita que no se vean algunas columnas
#print(surveys_df.columns)
#¿Cuántos individuos son hembras F y cuántos son machos M?
grouped_data = surveys_df.groupby('sex').size()
#print(grouped_data)
#agrupar por sexo y peso
"""grouped_data = surveys_df.groupby('sex')['weight'].mean()
print(grouped_data)
#print(grouped_data.mean())
print(grouped_data.plot(kind= 'bar'));
plt.show()"""
#Qué pasa cuando agrupas sobre dos columnas usando el siguiente enunciado y después tomas los valores medios:
grouped_data2 = surveys_df.groupby(['plot_id','sex'])
#print(grouped_data2.mean())
#Calcula las estadísticas descriptivas del peso para cada sitio. Sugerencia: puedes usar la siguiente sintaxis para solo crear las estadísticas para una columna de tus datos
peso_x_sitio = surveys_df.groupby( "plot_id" )
print(peso_x_sitio["weight"].describe())
