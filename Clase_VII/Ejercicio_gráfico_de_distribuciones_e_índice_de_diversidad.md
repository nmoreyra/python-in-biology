#### Distribuciones

Cree un nuevo DataFrame uniendo los contenidos de survey.csv y Tablas species.csv. Luego calcula y crea un gráfico de la distribución de:

1.  taxa por parcela
2.  taxa por sexo por parcela


#### Índice de Diversidad

1.  En la carpeta de datos, hay un gráfico **CSV** que contiene información sobre el tipo asociado con cada parcela. Usa esos datos para resumir el número de parcelas por tipo de parcela.
2.  Calcula un índice de diversidad de su elección para control vs parcelas de rodamiento de roedores El índice debe considerar tanto la abundancia de especies como el número de especies. Puedes optar por utilizar el simple índice de biodiversidad [descrito aquí](https://www.amnh.org/learn-teach/curriculum-collections/biodiversity-counts/plant-ecology/how-to-calculate-a-biodiversity-index) que calcula la diversidad como:

`el número de especies en la parcela / el número total de individuos en la parcela = índice de biodiversidad`